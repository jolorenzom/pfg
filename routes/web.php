<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Rutas sin identificacion
//Anonimo
Route::get('/',       'Anonimo\AnonimoController@index');
Route::get('/logout', 'Anonimo\AnonimoController@logout');
//Centros
Route::get('/centros',              'Centros\CentrosController@index');
Route::get('/centro/{id_centro}',    'Centros\CentrosController@GetCentroById');
//Anonimo-Reservas
Route::get('/reservas',        'Anonimo\AnonimoReservasController@GetAllReservas');
Route::post('/reserva_centro', 'Anonimo\AnonimoReservasController@GetReservaByCentroID');
//Rutas para identificarse
Auth::routes();
//Rutas con identificacion. Redirige a cada una de las HOME segun el perfil que haya hecho login
Route::get('/home', 'HomeController@index');
//Admin
Route::get('/editar_admin/{id_usuario}',  'Admin\AdminController@edit');
Route::post('/editar_admin/{id_usuario}', 'Admin\AdminController@update');
//Admin-Centros
Route::get('/alta_centro',                       'Centros\CentrosController@create');
Route::post('/alta_centro',                      'Centros\CentrosController@store');
Route::get('/eliminar_centro/{id_centro}',       'Centros\CentrosController@destroy');
Route::get('/editar_centro/{id_centro}',         'Centros\CentrosController@edit');
Route::post('/editar_centro/{id_centro}',        'Centros\CentrosController@update');
//Admin-Enfermeros
Route::get('/enfermeros',                              'Enfermeros\EnfermerosController@index');
Route::post('/enfermeros_centro',                      'Enfermeros\EnfermerosController@GetEnfermerosByIdCentro');
Route::post('/enfermeros',                             'Enfermeros\EnfermerosController@GetEnfermerosFiltrados');
Route::get('/enfermero/{id_enfermeros}',               'Enfermeros\EnfermerosController@GetEnfermeroById');
Route::get('/alta_enfermero',                          'Enfermeros\EnfermerosController@create');
Route::post('/alta_enfermero',                         'Enfermeros\EnfermerosController@store');
Route::get('/eliminar_enfermero/{id_enfermero}',       'Enfermeros\EnfermerosController@destroy');
Route::get('/editar_enfermero/{id_enfermero}',         'Enfermeros\EnfermerosController@edit');
Route::post('/editar_enfermero/{id_enfermero}',  'Enfermeros\EnfermerosController@update');
//Donantes
Route::get('/donantes',                     'Donantes\DonantesController@index');
Route::post('/donantes',                    'Donantes\DonantesController@GetDonantesFiltrados');
Route::get('/donante/{id_donante}',         'Donantes\DonantesController@GetDonanteById');
Route::get('/alta_donante',                 'Donantes\DonantesController@create');
Route::post('/alta_donante',                'Donantes\DonantesController@store');
Route::get('/editar_donante/{id_donante}',  'Donantes\DonantesController@edit');
Route::post('/editar_donante/{id_donante}', 'Donantes\DonantesController@update');
//Donaciones
Route::get('/donaciones',           'Donaciones\DonacionesController@index');
Route::post('/donaciones_donante',  'Donaciones\DonacionesController@GetDonacionesDonanteByDNI');
Route::post('/alta_donacion',       'Donaciones\DonacionesController@store');
//Donante-Donaciones
Route::get('/donante-donaciones', 'Donaciones\DonacionesController@GetDonacionesDonanteByUser');
//Transfusiones
Route::get('/transfusiones',     'Transfusiones\TransfusionesController@index');
Route::get('/alta_transfusion',  'Transfusiones\TransfusionesController@create');
Route::post('/alta_transfusion', 'Transfusiones\TransfusionesController@store');


