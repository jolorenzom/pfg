@extends('layouts.master')
@section('titulo', 'Lista de donaciones')

@section('contenido')
  <div class="bottom-margin">
      <ul class="nav nav-tabs nav-justified">
          <li role="presentation">
              <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
          </li>
          <li role="presentation" class="active">
              <a href="/donante-donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
          </li>
      </ul>
  </div>

  @if(isset($alert) and $alert!='')
    <div class="alert alert-danger alert-dismissable fade in" role="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ $alert }}
    </div>
  @endif
  
  @if(isset($success) and $success!='')
    <div class="alert alert-success alert-dismissable fade in" role="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ $success }}
    </div>
  @endif

  @if(Session::has('success'))
      <div class="alert alert-success alert-dismissable fade in" role="alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ Session::get('success') }}
      </div>
  @endif

  @if(Session::has('alert'))
    <div class="alert alert-danger alert-dismissable fade in" role="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('alert') }}
    </div>
  @endif

  <table class="table table-responsive table-hover">
    <thead class="bg-primary">
      <tr>
        <th class="centrar-txt">Centro</th>
        <th class="centrar-txt">Cantidad</th>
        <th class="centrar-txt">Grupo sanguíneo</th>
        <th class="centrar-txt">Fecha</th>
      </tr>
    </thead>
    <tbody>
      @foreach($donaciones as $donacion)
        <tr class="centrar-txt">
          <td> <a href="/centro/{{ $donacion -> id_centro }}"> {{ $donacion -> nombre_centro }}</a> </td>
          <td> {{ $donacion -> cantidad }} </td>
          <td> {{ $donacion -> grupo_sanguineo }} </td>
          <td> {{ $donacion -> fecha }} </td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection


