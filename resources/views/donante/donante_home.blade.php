@extends('layouts.master')
@section('titulo', 'Donante')

@section('contenido')
  <div class="bottom-margin">
      <ul class="nav nav-tabs nav-justified">
          <li role="presentation" class="active">
              <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
          </li>
          <li role="presentation">
              <a href="/donante-donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
          </li>
      </ul>
  </div>
  @if(Session::has('success'))
      <div class="alert alert-success alert-dismissable fade in" role="alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ Session::get('success') }}
      </div>
  @endif

  @if(Session::has('alert'))
    <div class="alert alert-danger alert-dismissable fade in" role="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('alert') }}
    </div>
  @endif

  <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Datos personales</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
              <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-4">
                  <li class="list-group-item">
                      Nombre: {{ $usuario_logado -> nombre }}
                  </li>
                  <li class="list-group-item">
                      Apellidos:  {{ $usuario_logado -> apellidos }}
                  </li>
                  <li class="list-group-item">
                      Email: {{ $usuario_logado -> email }}
                  </li>
                  <li class="list-group-item">
                      Tipo: {{ $usuario_logado -> tipo }}
                  </li>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4">
                  <li class="list-group-item">
                      Grupo sanguíneo: {{ $donante -> grupo_sanguineo }}
                  </li>
                  <li class="list-group-item">
                      Dni:  {{ $donante -> dni }}
                  </li>
                  <li class="list-group-item">
                      Teléfono: {{ $donante -> telefono }}
                  </li>
                  <li class="list-group-item">
                      Fecha nacimiento: {{ $donante -> fecha_nacimiento }}
                  </li>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                  <li class="list-group-item">
                      Sexo: {{ $donante -> sexo }}
                  </li>
                  <li class="list-group-item">
                      Dirección:  {{ $donante -> direccion }}
                  </li>
                  <li class="list-group-item">
                      Municipio: {{ $donante -> municipio }}
                  </li>
                  <li class="list-group-item">
                      Código postal: {{ $donante -> cp }}
                  </li>
                </div>
              </div>
            </ul>
            <a class="btn btn-info" href="/editar_donante/{{ $donante -> id_donante }}">Editar</a>
        </div>
    </div>
@endsection
