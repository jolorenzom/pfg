<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@yield('titulo')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet"     href="{{ URL::asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet"     href="{{ URL::asset('css/bootstrap-theme.min.css') }}">
        <link rel="stylesheet"     href="{{ URL::asset('css/app.css') }}">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">

        <script src="{{ URL::asset('js/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('js/app.js') }}"></script>        
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="container-fluid">
            <div class="row">
                <nav id="mainNav" class="navbar navbar-default top-margin">
                    <div class="col-xs-4 col-sm-6">   
                        <div class="">
                          <a class="navbar-brand color-white" href="/"> <img width="85" height="75" src="{{ URL::asset('img/ETSISI_logo.png')}}"></a>
                        </div>
                    </div> 
                    <div class="col-xs-8 col-sm-6">     
                        <ul class="nav navbar-nav pull-right equal-margin">
                            @if (Auth::guest())
                                    <li><a href="{{ url('/login') }}"><span class="glyphicon glyphicon-off color-white"></span> <span class="color-white">Login</span></a></li>
                                @else
                                    <li>
                                        <a href="/home"><span class="color-white">Hola {{ Auth::user()-> nombre }}, eres {{ Auth::user()-> tipo }} </span></a>
                                    </li>                        
                                    <li>
                                        <a href="{{ url('/logout') }}" onclick="event.preventDefault(); 
                                            document.getElementById('logout-form').submit();">
                                        <span class="glyphicon glyphicon-eject color-white"></span>
                                        <span class="color-white">Logout</span></a></li>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                            @endif  
                        </ul>    
                    </div>     
                </nav>
            </div>
        </div>
         </div>
        </div>

    	<div class="container">
            <div class="row">
                @yield('contenido')
            </div>   
        </div>

        <div class="container-fluid">
            <div class="row">
                <footer class="myFooter">
                    <div class="col-xs-10 col-sm-8 col-md-8 col-lg-9"> 
                        <p class="navbar-text">Sistema web de gestión de una red de centros de donación/transfusión de sangre</p>
                    </div>
                    <div class="col-xs-2 col-sm-4 col-md-4 col-lg-3">
                        <p class="navbar-text"><?php echo date('Y') ?></p>
                    </div>
                </footer>
            </div>
        </div>

    </body>
</html>