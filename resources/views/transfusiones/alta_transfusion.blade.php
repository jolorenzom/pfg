@extends('layouts.master')
@section('titulo', 'Alta de transfusión')

@section('contenido')
	<div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" >
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            <li role="presentation">
                <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
            </li>

            @if(Auth::user()-> tipo == 'administrador')
                <li role="presentation">
                	<a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
                </li>
                <li role="presentation">
                    <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                </li>
            @endif

            <li role="presentation">
                <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
            </li>
            <li role="presentation" class="active">
                <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
            </li>
        </ul>
    </div>

    @if(Session::has('message'))
        <div class="alert alert-danger alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}
        </div>
    @endif

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('success') }}
        </div>
    @endif

	<div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Alta</h3>
        </div>
        <div class="panel-body">
            <form action="/alta_transfusion" method="POST">
                {{ csrf_field() }}               
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="cantidad"> Cantidad </label>
                            <input type="number" class="form-control" name="cantidad" id="cantidad" min="1" required>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="id_centro"> Centro </label>
							<select class="form-control" name="id_centro" id="id_centro">
                                @foreach($centros as $centro)
									<option value="{{ $centro -> id_centro }}" selected> {{ $centro -> nombre }} </option>
								@endforeach
                            </select>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                        	<label for="grupo_sanguineo"> Grupo sanguíneo </label>
                            <select class="form-control" name="grupo_sanguineo" id="grupo_sanguineo">
								<option value="0+" selected>0+</option>
								<option value="0-">0-</option>
								<option value="A+">A+</option>
								<option value="A-">A-</option>
								<option value="B+">B+</option>
								<option value="B-">B-</option>
								<option value="AB+">AB+</option>
								<option value="AB-">AB-</option>
                            </select>
                        </div>
                    </div> 
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="fecha"> Fecha </label>
                            <input type="fecha" class="form-control" name="fecha" id="fecha" value="{{$fecha_actual}}" readonly>
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="observaciones"> Observaciones </label>
							<textarea style="resize:none;" class="form-control" rows="3" name="observaciones"> </textarea>
                        </div>
                    </div> 
                </div> 
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                        	<button type="submit" onclick="return confirm('¿Realmente deseas dar de alta el registro?');" class="btn btn-info">Enviar</button>
                        </div>
                    </div>                                      
                </div> 
            </form>
        </div>
    </div> 
@endsection