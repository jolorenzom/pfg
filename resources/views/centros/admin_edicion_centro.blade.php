@extends('layouts.master')
@section('titulo', 'Edición de Centro')

@section('contenido')
	<div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation">
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            <li role="presentation">
                <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
            </li>
            <li role="presentation">
                <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
            </li>
            <li role="presentation" class="active">
                <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
            </li>
            <li role="presentation">
                <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
            </li>
            <li role="presentation">
                <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
            </li>
        </ul>
    </div>


    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Datos a editar</h3>
        </div>
        <div class="panel-body">
            <form action="/editar_centro/{{ $centro -> id_centro }}" method="POST">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="nombre"> Nombre </label>
                            <input type="text" class="form-control" name="nombre" id="nombre" value="{{$centro -> nombre}}" required>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="direccion">Dirección</label>
                            <input type="text" class="form-control" name="direccion" id="direccion" value="{{$centro -> direccion}}" required>
                        </div>
                    </div>  
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="municipio"> Municipio </label>
                            <input type="municipio" class="form-control" name="email" id="municipio" value="{{$centro -> municipio}}"required>
                        </div>
                    </div> 
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="cp"> Código postal </label>
                            <input type="cp" class="form-control" name="email" id="cp" value="{{$centro -> cp}}" required>
                        </div>
                    </div>                      
                </div>  

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label> Ubicación sala donaciones </label>
							<textarea style="resize:none;" class="form-control" rows="2" name="ubicacion_sala_donaciones"> {{$centro -> ubicacion_sala_donaciones}}</textarea>
                        </div>
                    </div>    
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label> Horario donaciones </label>
							<textarea style="resize:none;" class="form-control" rows="2" name="horario_donaciones"> {{$centro -> horario_donaciones}} </textarea>
                        </div>
                    </div>                                    
                </div>  

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="telefono"> Teléfono </label>
                            <input type="text" class="form-control" name="telefono" id="telefono" value="{{$centro -> telefono}}">
                        </div>
                    </div>    
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="telefono_donaciones"> Teléfono sala donaciones </label>
                            <input type="text" class="form-control" name="telefono_donaciones" id="telefono_donaciones" value="{{$centro -> telefono_donaciones}}">
                        </div>
                    </div>                                      
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="email"> Email </label>
                            <input type="email" class="form-control" name="email" id="email" value="{{$centro -> email}}">
                        </div>
                    </div>    
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="web"> Web </label>
                            <input type="text" class="form-control" name="web" id="web" value="{{$centro -> web}}">
                        </div>
                    </div>                                      
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label> Observaciones </label>
							<textarea style="resize:none;" class="form-control" rows="3" name="horario_donaciones">  {{$centro -> observaciones}} </textarea>
                        </div>
                    </div> 
                </div>   
        		<button type="submit" onclick="return confirm('¿Realmente deseas editar el registro?');" class="btn btn-info">Enviar</button>
            </form>
        </div>
    </div>
@endsection
