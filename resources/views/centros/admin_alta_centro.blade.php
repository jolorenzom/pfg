@extends('layouts.master')
@section('titulo', 'Alta de Centro')

@section('contenido')
	<div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation">
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            <li role="presentation">
                <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
            </li>
            <li role="presentation">
                <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
            </li>
            <li role="presentation" class="active">
                <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
            </li>
            <li role="presentation">
                <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
            </li>
            <li role="presentation">
                <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
            </li>
        </ul>
    </div>

    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Alta</h3>
        </div>
        <div class="panel-body">
            <form action="/alta_centro" method="POST">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="nombre"> Nombre </label>
                            <input type="text" class="form-control" name="nombre" id="nombre" required>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="direccion">Dirección</label>
                            <input type="text" class="form-control" name="direccion" id="direccion" required>
                        </div>
                    </div>  
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="municipio"> Municipio </label>
                            <input type="municipio" class="form-control" name="municipio" id="municipio" required>
                        </div>
                    </div> 
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="cp"> Código postal </label>
                            <input type="cp" class="form-control" name="cp" id="cp" required>
                        </div>
                    </div>                      
                </div>  

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label> Ubicación sala donaciones </label>
							<textarea style="resize:none;" class="form-control" rows="2" name="ubicacion_sala_donaciones"> </textarea>
                        </div>
                    </div>    
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label> Horario donaciones </label>
							<textarea style="resize:none;" class="form-control" rows="2" name="horario_donaciones"> </textarea>
                        </div>
                    </div>                                    
                </div>  

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="telefono"> Teléfono </label>
                            <input type="text" class="form-control" name="telefono" id="telefono" >
                        </div>
                    </div>    
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="telefono_donaciones"> Teléfono sala donaciones </label>
                            <input type="text" class="form-control" name="telefono_donaciones" id="telefono_donaciones" >
                        </div>
                    </div>                                      
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="email"> Email </label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                    </div>    
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="web"> Web </label>
                            <input type="text" class="form-control" name="web" id="web">
                        </div>
                    </div>                                      
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="observaciones"> Observaciones </label>
							<textarea style="resize:none;" class="form-control" rows="3" name="observaciones"> </textarea>
                        </div>
                    </div> 
                </div>   

        		<button type="submit" onclick="return confirm('¿Realmente deseas dar de alta el registro?');" class="btn btn-info">Enviar</button>                                       
            </form>
        </div>
    </div>
@endsection
