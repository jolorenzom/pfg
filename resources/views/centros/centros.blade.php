@extends('layouts.master')
@section('titulo', 'Lista de centros')

@section('contenido')

    @if(Auth::user()!='')
        @if(Auth::user()-> tipo == 'administrador')
            <div class="bottom-margin">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                    </li>
                    <li role="presentation">
                        <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                    </li>
                    <li role="presentation">
                        <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
                    </li>
                    <li role="presentation" class="active">
                        <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                    </li>
                    <li role="presentation">
                        <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                    </li>
                    <li role="presentation">
                        <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                    </li>
                </ul>
            </div>
        @endif

        @if(Auth::user()-> tipo == 'enfermero')
            <div class="bottom-margin">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                    </li>
                    <li role="presentation">
                        <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                    </li>
                    <li role="presentation">
                        <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                    </li>
                    <li role="presentation">
                        <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                    </li>
                </ul>
            </div>
        @endif

        @if(Auth::user()-> tipo == 'donante')
            <div class="bottom-margin">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                    </li>
                    <li role="presentation">
                        <a href="/donante-donaciones"> Donaciones </a>
                    </li>
                </ul>
            </div>
        @endif
    @endif

    @if(Session::has('alert'))
        <div class="alert alert-danger alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('alert') }}
        </div>
    @endif

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('success') }}
        </div>
    @endif

    <table class="table table-responsive table-hover">
        <thead class="bg-primary">
            <tr>
                <th class="centrar-txt">Nombre</th>
                <th class="centrar-txt">Municipio</th>
                <th class="centrar-txt">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($centros as $centro)
                <tr class="centrar-txt">
                    <td> {{$centro-> nombre}} </td>
                    <td>{{$centro-> municipio}} </td>
                    <td>                    
                        <a href="/centro/{{ $centro-> id_centro }}"><span class="glyphicon glyphicon-eye-open side-padding" aria-hidden="true"></span></a> 
                        @if($usuario-> tipo == 'administrador')
                            <a href="/editar_centro/{{ $centro -> id_centro }}"><span class="glyphicon glyphicon-pencil side-padding" aria-hidden="true"></span></a> 
                            <a href="/eliminar_centro/{{ $centro -> id_centro }}"><span class="glyphicon glyphicon-trash side-padding" aria-hidden="true" onclick="return confirm('¿Realmente deseas eliminar el registro?');"></span></a>
                        @endif 
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    @if($usuario-> tipo == 'administrador')
        <a class="btn btn-primary" href="/alta_centro"> Dar de alta </a>
    @endif
@endsection
