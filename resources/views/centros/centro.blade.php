@extends('layouts.master')
@section('titulo', 'Info de Centro')

@section('contenido')
	@if(Auth::user()!='')
      @if(Auth::user()-> tipo == 'administrador')
          <div class="bottom-margin">
              <ul class="nav nav-tabs nav-justified">
                  <li role="presentation">
                      <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                  </li>
                  <li role="presentation">
                      <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                  </li>
                  <li role="presentation">
                      <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
                  </li>
                  <li role="presentation" class="active">
                      <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                  </li>
                  <li role="presentation">
                      <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                  </li>
                  <li role="presentation">
                      <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                  </li>
              </ul>
          </div>
      @endif

      @if(Auth::user()-> tipo == 'enfermero')
          <div class="bottom-margin">
              <ul class="nav nav-tabs nav-justified">
                  <li role="presentation">
                      <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                  </li>
                  <li role="presentation">
                      <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                  </li>
                  <li role="presentation">
                    <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                  </li>
                  <li role="presentation">
                      <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                  </li>
              </ul>
          </div>
      @endif

      @if(Auth::user()-> tipo == 'donante')
          <div class="bottom-margin">
              <ul class="nav nav-tabs nav-justified">
                  <li role="presentation">
                      <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                  </li>
                  <li role="presentation">
                      <a href="/donante-donaciones"> Donaciones </a>
                  </li>
              </ul>
          </div>
      @endif
  @endif


    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Datos</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
              <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-4">
                  <li class="list-group-item">
                      Nombre: {{ $centro-> nombre }}
                  </li>
                  <li class="list-group-item">
                      Dirección:  {{ $centro-> direccion }}
                  </li>
                  <li class="list-group-item">
                      Municipio: {{ $centro-> municipio }}
                  </li>
                  <li class="list-group-item">
                      Código postal: {{ $centro-> cp }}
                  </li>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4">
                  <li class="list-group-item">
                      Teléfono: {{ $centro-> telefono }}
                  </li>
                  <li class="list-group-item">
                      Horario donaciones:  {{ $centro-> horario_donaciones }}
                  </li>
                  <li class="list-group-item">
                      Ubicación sala donaciones: {{ $centro-> ubicacion_sala_donaciones }}
                  </li>
                  <li class="list-group-item">
                      Teléfono donaciones: {{ $centro-> telefono_donaciones }}
                  </li>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                  <li class="list-group-item">
                      Email: {{ $centro-> email }}
                  </li>
                  <li class="list-group-item">
                      Web:  {{ $centro-> web }}
                  </li>
                  <li class="list-group-item">
                      Observaciones: {{ $centro-> observaciones }} 
                  </li>
                </div>
              </div>
            </ul>
        </div>
    </div>
      @if(Auth::user()=='' or Auth::user()-> tipo != 'administrador')
        <div class="">
            <a class="btn btn-primary" href="/centros"> Centros</a> 
        </div>
      @endif

@endsection
