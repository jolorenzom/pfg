<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
   </head>
   <body>
      <h1>Datos del donante</h1>
      <p>Nombre y Apellidos: {{$nombre}} {{$apellidos}}</p>
      <p>Tipo de usuario: {{$tipo}}</p>
      <p>Correo electrónico: {{$email}}</p>
      <p>Contraseña: {{$password}}</p>
      <p>Grupo sanguíneo: {{$grupo_sanguineo}}</p>
      <p>Dni: {{$dni}}</p>
      <p>Teléfono: {{$telefono}}</p>
      <p>Fecha de nacimiento: {{$fecha_nacimiento}}</p>
      <p>Sexo: {{$sexo}}</p>
      <p>Dirección, municipio y código postal: {{$direccion}}, {{$municipio}} - {{$cp}}</p>
   </body>
</html>