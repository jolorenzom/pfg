@extends('layouts.master')
@section('titulo', 'Lista de enfermeros')

@section('contenido')
    <div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" >
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            @if(Auth::user()-> tipo == 'administrador')
                <li role="presentation">
                    <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                </li>
                <li role="presentation" class="active">
                    <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a>
                </li>
                <li role="presentation">
                    <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                </li>
                <li role="presentation">
                    <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                </li>
                <li role="presentation">
                    <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                </li>
            @endif
        </ul>
    </div>
    <table class="table table-responsive table-hover">
        <thead class="bg-primary">
            <tr>
                <th class="centrar-txt">Nombre</th>
                <th class="centrar-txt">Email</th>
                <th class="centrar-txt">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($enfermeros as $enfermero)
                <tr class="centrar-txt">
                    <td>{{ $enfermero-> nombre }} </td>
                    <td>{{ $enfermero-> email }} </td>
                    <td>                    
                        <a href="/enfermero/{{ $enfermero -> id_enfermero }}"><span class="glyphicon glyphicon-eye-open side-padding" aria-hidden="true"></span></a> 
                        <a href="/editar_enfermero/{{ $enfermero -> id_enfermero }}"><span class="glyphicon glyphicon-pencil side-padding" aria-hidden="true"></span></a> 
                        <a href="/eliminar_enfermero/{{ $enfermero -> id_enfermero }}"><span class="glyphicon glyphicon-trash side-padding" aria-hidden="true" onclick="return confirm('¿Realmente deseas eliminar el registro?');"></span></a> 
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection