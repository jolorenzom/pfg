@extends('layouts.master')
@section('titulo', 'Alta de Enfermero')

@section('contenido')
	<div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" >
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            @if(Auth::user()-> tipo == 'administrador')
                <li role="presentation">
                    <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                </li>
                <li role="presentation" class="active">
                    <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a>
                </li>
                <li role="presentation">
                    <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                </li>
                <li role="presentation">
                    <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                </li>
                <li role="presentation">
                    <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                </li>
            @endif
        </ul>
    </div>

    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Alta</h3>
        </div>
        <div class="panel-body">
            <form action="/alta_enfermero" method="POST">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="nombre"> Nombre </label>
                            <input type="text" class="form-control" name="nombre" id="nombre" required>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" id="apellidos" required>
                        </div>
                    </div>  
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="email"> Email </label>
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>
                    </div> 
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="sexo"> Centro </label>
							<select class="form-control" name="id_centro" id="id_centro">
								<option value="vacio" selected>Seleccione una opción...</option>
                                @foreach($centros as $centro)
                                    <option value="{{ $centro -> id_centro }}" > {{ $centro -> nombre }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                        	<button type="submit" onclick="return confirm('¿Realmente deseas dar de alta el registro?');" class="btn btn-info">Enviar</button>
                        </div>
                    </div>                                      
                </div> 
            </form>
        </div>
    </div>
@endsection