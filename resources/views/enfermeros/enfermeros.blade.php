@extends('layouts.master')
@section('titulo', 'Enfermeros')

@section('contenido')
    
    <div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" >
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            @if(Auth::user()-> tipo == 'administrador')
                <li role="presentation">
                    <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                </li>
                <li role="presentation" class="active">
                    <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a>
                </li>
                <li role="presentation">
                    <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                </li>
                <li role="presentation">
                    <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                </li>
                <li role="presentation">
                    <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                </li>
            @endif
        </ul>
    </div>

    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}
        </div>
    @endif

    @if(Session::has('alert'))
        <div class="alert alert-danger alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('alert') }}
        </div>
    @endif

    @if(Session::has('success'))
      <div class="alert alert-success alert-dismissable fade in" role="alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ Session::get('success') }}
      </div>
    @endif

    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Datos a filtrar</h3>
        </div>
        <div class="panel-body">
            <form action="/enfermeros" method="POST">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="nombre"> Nombre </label>
                            <input type="text" class="form-control" name="nombre" id="nombre">
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" id="apellidos">
                        </div>
                    </div>  
                </div> 
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="email"> Email </label>
                            <input type="text" class="form-control" name="email" id="email">
                        </div>
                    </div> 
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label class="color-white"> Código postal </label>
                            <div>
                                <button type="submit" class="btn btn-info">Filtrar</button>
                            </div>
                        </div>
                    </div>                    
                </div>                
            </form>
        </div>
    </div>

    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Filtrar por centros</h3>
        </div>
        <div class="panel-body">
            <form action="/enfermeros_centro" method="POST">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="id_centro" class="color-white"> Centro </label>
                            <select class="form-control" name="id_centro" id="id_centro">
                                @foreach($centros as $centro)
                                    <option value="{{ $centro -> id_centro }}" > {{ $centro -> nombre }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>   

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label class="color-white"> ----- </label>
                            <div>
                                <button type="submit" class="btn btn-info">Filtrar</button>
                            </div>
                        </div>
                    </div>   
                </div>                
                
            </form>
        </div>
    </div>

    <div class="text-center">
        <a class="btn btn-primary" href="/alta_enfermero"> Dar de alta</a> 
    </div>
@endsection