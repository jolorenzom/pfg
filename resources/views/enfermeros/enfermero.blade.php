@extends('layouts.master')
@section('titulo', 'Info de Enfermero')

@section('contenido')
	<div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" >
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            @if(Auth::user()-> tipo == 'administrador')
                <li role="presentation">
                    <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                </li>
                <li role="presentation" class="active">
                    <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a>
                </li>
                <li role="presentation">
                    <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                </li>
                <li role="presentation">
                    <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                </li>
                <li role="presentation">
                    <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                </li>
            @endif
        </ul>
    </div>
    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Datos</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                    	<li class="list-group-item list-group-item-info centrar-txt top-margin">
                            Personales
                        </li>
                        <li class="list-group-item">
                            Nombre: {{ $usuario -> nombre }}
                        </li>
                        <li class="list-group-item">
                            Apellidos: {{ $usuario -> apellidos }}
                        </li>
                        <li class="list-group-item">
                            Email: {{ $usuario -> email }}
                        </li>
                    </div> 
                    <div class="col-sm-6 col-md-6 col-lg-6">
                    	<li class="list-group-item list-group-item-info centrar-txt top-margin">
                            Centro
                        </li>
                        <li class="list-group-item">
                            Nombre: {{ $centro -> nombre }}
                        </li>
                        <li class="list-group-item">
                            Dirección: {{ $centro -> direccion }}
                        </li>
                        <li class="list-group-item">
                            Municipio: {{ $centro -> municipio }}
                        </li>
                    </div>                     
                </div>
            </ul>
        </div>
    </div> 
@endsection
