@extends('layouts.master')
@section('titulo', 'Reservas de sangre por centro')

@section('contenido') 
    
    @if(Auth::user()!='')
        @if(Auth::user()-> tipo == 'administrador')
            <div class="bottom-margin">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                    </li>
                    <li role="presentation">
                        <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                    </li>
                    <li role="presentation">
                        <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
                    </li>
                    <li role="presentation">
                        <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                    </li>
                    <li role="presentation">
                        <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                    </li>
                    <li role="presentation">
                        <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                    </li>
                </ul>
            </div>
        @endif

        @if(Auth::user()-> tipo == 'enfermero')
            <div class="bottom-margin">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                    </li>
                    <li role="presentation">
                        <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                    </li>
                    <li role="presentation">
                        <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                    </li>
                    <li role="presentation">
                        <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                    </li>
                </ul>
            </div>
        @endif

        @if(Auth::user()-> tipo == 'donante')
            <div class="bottom-margin">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                    </li>
                    <li role="presentation">
                        <a href="/donante-donaciones"> Donaciones </a>
                    </li>
                </ul>
            </div>
        @endif
    @endif

    @if(Session::has('message'))
        <div class="alert alert-alert alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"> Reservas de sangre: {{$reservas -> nombre_centro}}</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <div class="row">
                    @foreach($reservas as $reserva)
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            @if($reserva->cantidad<4)
                                <li class="list-group-item list-group-item-danger">
                            @elseif($reserva->cantidad<7)
                                <li class="list-group-item list-group-item-warning">
                            @else
                                <li class="list-group-item list-group-item-success">
                            @endif
                                <span>{{ $reserva-> grupo_sanguineo }}</span>
                                <span class="badge">{{$reserva-> cantidad}}</span> 
                            </li>
                        </div>
                    @endforeach
                </div>
            </ul>
        </div>
    </div>

    <div class="">
        <a class="btn btn-primary" href="/reservas"> Reservas totales</a> 
    </div>
@endsection
