@extends('layouts.master')
@section('titulo', 'Reservas de sangre')

@section('contenido')
    
    @if(Auth::user()!='')
        @if(Auth::user()-> tipo == 'administrador')
            <div class="bottom-margin">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                    </li>
                    <li role="presentation">
                        <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                    </li>
                    <li role="presentation">
                        <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
                    </li>
                    <li role="presentation">
                        <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                    </li>
                    <li role="presentation">
                        <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                    </li>
                    <li role="presentation">
                        <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                    </li>
                </ul>
            </div>
        @endif

        @if(Auth::user()-> tipo == 'enfermero')
            <div class="bottom-margin">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                    </li>
                    <li role="presentation">
                        <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                    </li>
                    <li role="presentation">
                        <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                    </li>
                    <li role="presentation">
                        <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                    </li>
                </ul>
            </div>
        @endif

        @if(Auth::user()-> tipo == 'donante')
            <div class="bottom-margin">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                    </li>
                    <li role="presentation">
                        <a href="/donante-donaciones"> Donaciones </a>
                    </li>
                </ul>
            </div>
        @endif
    @endif

    @if(Session::has('message'))
        <div class="alert alert-alert alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"> Reservas de sangre totales</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        @for($i = 0; $i < 4; $i++)
                            @if($cantidades[$i]<10)
                                <li class="list-group-item list-group-item-danger">
                            @elseif ($cantidades[$i]<20)
                                <li class="list-group-item list-group-item-warning">
                            @else
                                <li class="list-group-item list-group-item-success">
                            @endif
                                <span>{{ $grupo_sanguineos[$i] }}</span>
                                <span class="badge">{{ $cantidades[$i] }}</span> 
                            </li>
                        @endfor
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        @for($i = 4; $i < 8; $i++)
                            @if($cantidades[$i]<10)
                                <li class="list-group-item list-group-item-danger">
                            @elseif ($cantidades[$i]<20)
                                <li class="list-group-item list-group-item-warning">
                            @else
                                <li class="list-group-item list-group-item-success">
                            @endif
                                <span>{{ $grupo_sanguineos[$i] }}</span>
                                <span class="badge">{{ $cantidades[$i] }}</span> 
                            </li>
                        @endfor
                    </div>
                </div>
            </ul>
        </div>
    </div> 

    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Filtrar por centros</h3>
        </div>
        <div class="panel-body">
            <form action="/reserva_centro" method="POST">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="id_centro" class="color-white"> Centro </label>
                            <select class="form-control" name="id_centro" id="id_centro">
                                @foreach($centros as $centro)
                                    <option value="{{ $centro -> id_centro }}" > {{ $centro -> nombre }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>   

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label class="color-white"> ----- </label>
                            <div>
                                <button type="submit" class="btn btn-info">Filtrar</button>
                            </div>
                        </div>
                    </div>   
                </div>                
                
            </form>
        </div>
    </div>
@endsection
