@extends('layouts.master')
@section('titulo', 'Donaciones donante')

@section('contenido')
	
	<div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" >
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            <li role="presentation">
                <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
            </li>

            @if(Auth::user()-> tipo == 'administrador')
                <li role="presentation">
                    <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
                </li>
                <li role="presentation">
                    <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                </li>
            @endif

            <li role="presentation" class="active">
                <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
            </li>
            <li role="presentation">
                <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones</a>
            </li>
        </ul>
    </div>

	@if(Session::has('message'))
        <div class="alert alert-warning alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Datos del donante</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <li class="list-group-item">
                            Nombre: {{ $usuario -> nombre }}
                        </li>
                        <li class="list-group-item">
                            Apellidos: {{ $usuario -> apellidos }}
                        </li>
                        <li class="list-group-item">
                            Email: {{ $usuario -> email }}
                        </li>
                    </div> 
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <li class="list-group-item">
                            Dni: {{ $donante -> dni }}
                        </li>
                        <li class="list-group-item">
                            Grupo sanguíneo: {{ $donante -> grupo_sanguineo }}
                        </li>
                    </div>                     
                </div>
            </ul>
        </div>
    </div> 

	<div class="panel panel-primary">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<h3 class="panel-title">Donaciones realizadas</h3>
		</div>
		<!-- Table -->
		<table class="table table-responsive table-hover">
	        <thead class="bg-info">
	            <tr>
	                <th class="centrar-txt">Centro</th>
	                <th class="centrar-txt">Cantidad</th>
	                <th class="centrar-txt">Fecha</th>
	            </tr>
	        </thead>
	        <tbody>
	            @foreach($donaciones as $donacion)
	                <tr class="centrar-txt">
	                    <td> {{ $donacion -> nombre_centro }} </td>
	                    <td> {{ $donacion -> cantidad }} </td>
	                    <td> {{ $donacion -> fecha }} </td>
	                </tr>
	            @endforeach
	        </tbody>
	    </table>
	</div>

	<div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Alta</h3>
        </div>
        <div class="panel-body">
            <form action="/alta_donacion" method="POST">
                {{ csrf_field() }}
                <div>
					<input type="hidden" name="id_donante" value="{{$donante -> id_donante}}" readonly>
				</div>
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="cantidad"> Cantidad </label>
                            <input type="number" class="form-control" name="cantidad" id="cantidad" min="1" required>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="id_centro"> Centro </label>
							<select class="form-control" name="id_centro" id="id_centro">
                                @foreach($centros as $centro)
									<option value="{{ $centro -> id_centro }}" selected> {{ $centro -> nombre }} </option>
								@endforeach
                            </select>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="fecha"> Fecha </label>
                            <input type="fecha" class="form-control" name="fecha" id="fecha" value="{{$fecha_actual}}" readonly>
                        </div>
                    </div> 
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="grupo_sanguineo"> Grupo sanguíneo </label>
                            <input type="grupo_sanguineo" class="form-control" name="grupo_sanguineo" id="grupo_sanguineo" value="{{$donante -> grupo_sanguineo}}" readonly>
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                        	<button type="submit" onclick="return confirm('¿Realmente deseas dar de alta el registro?');" class="btn btn-info">Enviar</button>
                        </div>
                    </div>                                      
                </div> 
            </form>
        </div>
    </div>
@endsection