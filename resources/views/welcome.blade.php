@extends('layouts.master')
@section('titulo', 'Inicio')

@section('contenido')
    <div> {{ Session::get('message') }} </div>
    
	<div class="jumbotron" style="color:white; background-color: #337ab7;">
		<div class="container">
			<h2 style="text-align: center; margin-top: 0; margin-bottom: 4%;">Sistema web de gestión de una red de centros de donación/transfusión de sangre</h2>
			<p style="text-align: justify;"> 
			Bienvenido al sistema web de gestión de una red de centros de donación/transfusión de sangre. Con él, podrás llevar un control de las reservas de sangre existentes, tanto a nivel general como por los centros de donación/transfusión.
			También podrás gestionar tu propio perfil de usuario, pudiendo editar (si procede) tus datos personales y tus credenciales de acceso.
			Otra funcionalidad será la posibilidad de gestionar tanto las donaciones como las transfusiones de sangre que se realizan.
			Por último, podrás consultar la información relativa a los centros de donación/transfusión de sangre que hay y, si tu perfil de usuario lo permite, podrás registrar, editar y consultar los datos de esos centros, así como de los donantes y enfermeros existentes.
			</p>
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div style="margin-bottom: 5%;">
						<a class="btn btn-info btn-lg" href="/centros" role="button" style="display:block;">Consultar centros</a>
					</div>
				</div>  
					<div class="col-sm-6 col-md-6 col-lg-6">
						<div style="margin-bottom: 5%;">
							<a class="btn btn-info btn-lg" href="/reservas" role="button" style="display:block;">Consultar reservas</a>
						</div>
				</div>
			</div>
		</div>
	</div>
@endsection
