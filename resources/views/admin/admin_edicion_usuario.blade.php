@extends('layouts.master')
@section('titulo', 'Edición de Usuario')

@section('contenido')
		<div class="bottom-margin">
	        <ul class="nav nav-tabs nav-justified">
	            <li role="presentation" class="active">
	                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
	            </li>
	            <li role="presentation">
	                <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
	            </li>
	            <li role="presentation">
	                <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
	            </li>
	            <li role="presentation">
	                <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
	            </li>
	            <li role="presentation">
	                <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
	            </li>
	            <li role="presentation">
	                <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
	            </li>
	        </ul>
	    </div>
		<div class="panel panel-responsive panel-primary">
			<div class="panel-heading">
			    <h3 class="panel-title">Datos a editar</h3>
			</div>
			<div class="panel-body">
				<form action="/editar_admin/{{ $usuario -> id_usuario }}" method="POST">
					{{ csrf_field() }}

					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label for="nombre"> Nombre </label>
								<input type="text" class="form-control" name="nombre" id="nombre" value="{{ $usuario -> nombre }}">
							</div>
						</div>

						<div class="col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label for="apellidos">Apellidos</label>
								<input type="text" class="form-control" name="apellidos" id="apellidos" value="{{ $usuario -> apellidos }}">
							</div>
						</div>  
					</div> 
					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label for="email"> Email </label>
								<input type="email" class="form-control" name="email" id="email" value="{{ $usuario -> email }}">
							</div>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6">
							<div class="collapse" id="edit-pass">
								<div class="form-group">
									<label for="email"> Contraseña </label>
									<input type="password" class="form-control" name="password" id="password" value="{{ $usuario -> password }}">
								</div>
							</div>					
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<button type="button" data-toggle="collapse" class="btn btn-warning" data-target="#edit-pass">Editar contraseña</button>
							</div>						
						</div>
					</div>
					
					<button type="submit" class="btn btn-info"onclick="return confirm('¿Realmente deseas editar el registro?');" >Enviar</button>
				</form>
			</div>
		</div> 
@endsection
