@extends('layouts.master')
@section('titulo', 'Admin')

@section('contenido')
    <div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" class="active">
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            <li role="presentation">
                <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
            </li>
            <li role="presentation">
                <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
            </li>
            <li role="presentation">
                <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
            </li>
            <li role="presentation">
                <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
            </li>
            <li role="presentation">
                <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
            </li>
        </ul>
    </div>

    @if(Session::has('success'))
      <div class="alert alert-success alert-dismissable fade in" role="alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ Session::get('success') }}
      </div>
    @endif

    @if(Session::has('alert'))
        <div class="alert alert-danger alert-dismissable fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('alert') }}
        </div>
    @endif
    
    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Datos personales</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <li class="list-group-item">
                    Nombre: {{ $usuario_logado -> nombre }}
                </li>
                <li class="list-group-item">
                    Apellidos:  
                    {{ $usuario_logado -> apellidos }}
                </li>
                <li class="list-group-item">
                    Email: {{ $usuario_logado -> email }}
                </li>
                <li class="list-group-item">
                    Tipo: {{ $usuario_logado -> tipo }}
                </li>
            </ul>
            <a class="btn btn-info" href="/editar_admin/{{ $usuario_logado -> id_usuario }}"> Editar</a>
        </div>
    </div>   
@endsection
