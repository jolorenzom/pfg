@extends('layouts.master')
@section('titulo', 'Lista de donantes')

@section('contenido')
    <div class="bottom-margin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" >
                <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
            </li>
            <li role="presentation" class="active">
                <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
            </li>

            @if(Auth::user()-> tipo == 'administrador')
                <li role="presentation">
                    <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a>
                </li>
                <li role="presentation">
                    <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                </li>
            @endif

            <li role="presentation">
                <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
            </li>
            <li role="presentation">
                <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
            </li>
        </ul>
    </div>
                           
    <table class="table table-responsive table-hover">
        <thead class="bg-primary">
            <tr>
                <th class="centrar-txt">Nombre</th>
                <th class="centrar-txt">Dni</th>
                <th class="centrar-txt">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($donantes as $donante)
                <tr class="centrar-txt">
                    <td>{{ $donante-> nombre }} </td>
                    <td>{{ $donante-> dni }} </td>
                    <td>
                        <a href="/donante/{{ $donante-> id_donante }}"><span class="glyphicon glyphicon-eye-open side-padding" aria-hidden="true"></span></a> 
                        <a href="/editar_donante/{{ $donante -> id_donante }}"><span class="glyphicon glyphicon-pencil side-padding" aria-hidden="true"></span></a> 
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
