@extends('layouts.master')
@section('titulo', 'Edición de donante')

@section('contenido')

    @if(Auth::user()-> tipo == 'donante')
        <div class="bottom-margin">
            <ul class="nav nav-tabs nav-justified">
                <li role="presentation" class="active">
                  <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                </li>
                <li role="presentation">
                  <a href="/donante-donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                </li>
            </ul>
        </div>
    @else
    	<div class="bottom-margin">
            <ul class="nav nav-tabs nav-justified">
                <li role="presentation" >
                    <a href="/home"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> </a>
                </li>
                <li role="presentation" class="active">
                    <a href="/donantes"> <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Donantes </a>
                </li>

                @if(Auth::user()-> tipo == 'administrador')
                    <li role="presentation">
                        <a href="/enfermeros"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Enfermeros </a> 
                    </li>
                    <li role="presentation">
                        <a href="/centros"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Centros </a>
                    </li>
                @endif

                <li role="presentation">
                    <a href="/donaciones"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Donaciones </a>
                </li>
                <li role="presentation">
                    <a href="/alta_transfusion"> <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Transfusiones </a>
                </li>
            </ul>
        </div>
    @endif

    <div class="panel panel-responsive panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Datos a editar</h3>
        </div>
        <div class="panel-body">
            <form action="/editar_donante/{{ $donante -> id_donante }}" method="POST">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="nombre"> Nombre </label>
                            <input type="text" class="form-control" name="nombre" id="nombre" value="{{ $usuario -> nombre }}"required>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" id="apellidos" value="{{ $usuario -> apellidos }}" required>
                        </div>
                    </div>  
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="email"> Email </label>
                            <input type="email" class="form-control" name="email" id="email" value="{{ $usuario -> email }}" required>
                        </div>
                    </div> 
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="dni"> Dni </label>
                            <input type="text" class="form-control" name="dni" id="dni" value="{{ $donante -> dni }}" required>
                        </div>
                    </div>                     
                </div>  

                <div class="row">
                	<div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="telefono"> Teléfono </label>
                            <input type="text" class="form-control" name="telefono" id="telefono" value="{{ $donante -> telefono }}">
                        </div>
                    </div>  
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="fecha_nacimiento"> Fecha nacimiento </label>
                            <input type="date" class="form-control" name="fecha_nacimiento" value="{{ $donante -> fecha_nacimiento }}" required>
                        </div>
                    </div>						
				</div>

                <div class="row">
                	<div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="sexo"> Sexo </label>
                            <select class="form-control" name="sexo" id="sexo" required>
                            	<option value="vacio">Seleccione una opción...</option>
								<option value="Mujer" >Mujer</option>
								<option value="Hombre">Hombre</option>
							</select>
                        </div>
                    </div>    
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="direccion"> Dirección </label>
                            <input type="text" class="form-control" name="direccion" id="direccion" value="{{ $donante -> direccion }}">
                        </div>
                    </div>                                                   
                </div>  

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="municipio"> Municipio </label>
                            <input type="text" class="form-control" name="municipio" id="municipio" value="{{ $donante -> municipio }}"> 
                        </div>
                    </div>  
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="cp"> Código postal </label>
                            <input type="text" class="form-control" name="cp" id="cp" value="{{ $donante -> cp }}">
                        </div>
                    </div>                                                             
                </div>

                <div class="row">
					<div class="col-sm-6 col-md-6 col-lg-6">
						<div class="collapse" id="edit-pass">
						<div class="form-group">
							<label for="password"> Contraseña </label>
							<input type="password" class="form-control" name="password" id="password">
						</div>
						</div>	
                	</div> 
                	<div class="col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
                       		<label class="color-white"> Código postal </label>
                        	<div>
								<button type="button" data-toggle="collapse" class="btn btn-warning" data-target="#edit-pass">Editar contraseña</button>
							</div>
						</div>						
					</div>                                
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                        	<button type="submit" onclick="return confirm('¿Realmente deseas editar el registro?');" class="btn btn-info">Enviar</button>
                        </div>
                    </div>                                      
                </div>         
            </form>
        </div>
    </div>
@endsection
