/*
 Navicat Premium Data Transfer

 Source Server         : Desarrollo
 Source Server Type    : MySQL
 Source Server Version : 50628
 Source Host           : localhost
 Source Database       : pfg

 Target Server Type    : MySQL
 Target Server Version : 50628
 File Encoding         : utf-8

 Date: 07/09/2017 00:28:29 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `centros`
-- ----------------------------
DROP TABLE IF EXISTS `centros`;
CREATE TABLE `centros` (
  `id_centro` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `municipio` varchar(255) DEFAULT NULL,
  `cp` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `horario_donaciones` text,
  `ubicacion_sala_donaciones` text,
  `telefono_donaciones` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `observaciones` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_centro`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `centros`
-- ----------------------------
BEGIN;
INSERT INTO `centros` VALUES ('4', 'Centro de Transfusión', 'Avda. de la Democracia s/n.', 'Madrid', '28028', '91 301 72 00', 'Este es el principal centro de donaciones/transfusiones de la Comunidad de Madrid ', 'Planta baja ', '900 30 35 30', 'centro.transfusion@salud.madrid.org', 'http://www.madrid.org', 'Parking gratuito', '2017-07-08 19:19:58', '2017-07-08 19:39:17'), ('5', 'Hospital Universitario Príncipe de Asturias', 'Carretera Alcalá-Meco, s/n', 'Alcalá de Henares', '28035', '91 887 81 00', 'Parking gratuito ', '1ª Planta, junto a paritorio', '91 887 81 00', 'gerent.hupa@salud.madrid.org', 'http://donarsangre.sanidadmadrid.org', 'Parking gratuito', '2017-07-08 19:28:25', '2017-07-08 19:38:59'), ('10', 'Hospital Universitario de La Princesa', 'C/ Diego de León, 62', 'Madrid', '28006', '91 520 22 00', 'Lunes a sábado de 8:30 a 21:30 h (no festivos)', '2ª Planta, Banco de Sangre', '91 520 35 67 / 91 520 22 41', 'atepac.hlpr@salud.madrid.org', 'www.madrid.org', ' ', '2017-07-08 19:58:39', null);
COMMIT;

-- ----------------------------
--  Table structure for `donaciones`
-- ----------------------------
DROP TABLE IF EXISTS `donaciones`;
CREATE TABLE `donaciones` (
  `id_donacion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_centro` int(10) unsigned NOT NULL,
  `id_donante` int(10) unsigned NOT NULL,
  `cantidad` int(10) unsigned DEFAULT NULL,
  `grupo_sanguineo` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_donacion`),
  KEY `id_centro` (`id_centro`),
  KEY `id_donante` (`id_donante`),
  CONSTRAINT `donaciones_ibfk_1` FOREIGN KEY (`id_centro`) REFERENCES `centros` (`id_centro`),
  CONSTRAINT `donaciones_ibfk_2` FOREIGN KEY (`id_donante`) REFERENCES `donantes` (`id_donante`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `donaciones`
-- ----------------------------
BEGIN;
INSERT INTO `donaciones` VALUES ('13', '10', '9', '10', '0+', '2017-07-08', '2017-07-08 21:05:57', null), ('14', '4', '10', '10', '0-', '2017-07-08', '2017-07-08 21:11:23', null), ('15', '5', '11', '10', 'A+', '2017-07-08', '2017-07-08 21:12:23', null), ('16', '4', '12', '10', 'A-', '2017-07-08', '2017-07-08 21:14:17', null), ('17', '10', '13', '10', 'B+', '2017-07-08', '2017-07-08 21:16:17', null), ('18', '5', '14', '10', 'B-', '2017-07-08', '2017-07-08 21:17:48', null), ('19', '4', '15', '10', 'AB+', '2017-07-08', '2017-07-08 21:18:35', null), ('20', '10', '16', '10', 'AB-', '2017-07-08', '2017-07-08 21:19:05', null), ('23', '4', '14', '10', 'B-', '2017-01-08', '2017-07-08 21:28:45', null), ('24', '4', '13', '10', 'B+', '2017-01-08', '2017-07-08 21:31:23', null), ('25', '4', '11', '10', 'A+', '2017-01-08', '2017-07-08 21:32:21', '2017-07-08 21:52:44'), ('26', '4', '9', '10', '0+', '2017-01-08', '2017-07-08 21:33:58', null), ('27', '4', '16', '10', 'AB-', '2017-01-08', '2017-07-08 21:35:20', '2017-07-08 21:35:25'), ('28', '5', '9', '10', '0+', '2016-01-08', '2017-07-08 21:37:13', null), ('29', '5', '10', '10', '0-', '2016-01-08', '2017-07-08 21:38:04', null), ('30', '5', '12', '10', 'A-', '2016-01-08', '2017-07-08 21:40:46', null), ('31', '5', '15', '10', 'AB+', '2016-01-08', '2017-07-08 21:42:53', null), ('32', '10', '11', '10', 'A+', '2015-01-08', '2017-07-08 21:45:36', null), ('33', '10', '12', '10', 'A-', '2015-01-08', '2017-07-08 21:46:35', '2017-07-08 21:46:48'), ('34', '10', '14', '10', 'B-', '2015-01-08', '2017-07-08 21:47:46', null);
COMMIT;

-- ----------------------------
--  Table structure for `donantes`
-- ----------------------------
DROP TABLE IF EXISTS `donantes`;
CREATE TABLE `donantes` (
  `id_donante` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) unsigned NOT NULL,
  `grupo_sanguineo` varchar(255) NOT NULL,
  `dni` varchar(255) NOT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `sexo` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `municipio` varchar(255) DEFAULT NULL,
  `cp` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_donante`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `donantes_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `donantes`
-- ----------------------------
BEGIN;
INSERT INTO `donantes` VALUES ('9', '54', '0+', '0123456789A', '0123456789', '1989-01-01', 'Mujer', 'Calle Irreal nº 1 ', 'Desembarco del Rey', '01234', '2017-07-08 20:27:02', '2017-07-08 20:44:54'), ('10', '55', '0-', '0123456789B', '0123456789', '1989-02-01', 'Hombre', 'Calle Irreal nº 2', 'El Muro', '01234', '2017-07-08 20:38:03', '2017-07-08 20:44:45'), ('11', '56', 'A+', '0123456789C', '0123456789', '1989-03-01', 'Hombre', 'Calle Irreal nº 3', 'Roca Casterly', '01234', '2017-07-08 20:43:54', null), ('12', '57', 'A-', '0123456789D', '0123456789', '1989-04-01', 'Mujer', 'Calle Irreal nº 4', 'Altojardin', '01234', '2017-07-08 20:47:09', null), ('13', '58', 'B+', '0123456789E', '0123456789', '1989-05-01', 'Hombre', 'Hodor Hodor nº 5', 'Hodor', '01234', '2017-07-08 20:48:25', null), ('14', '59', 'B-', '0123456789F', '0123456789', '1989-06-01', 'Mujer', 'Calle Irreal nº 6', 'Invernalia', '01234', '2017-07-08 20:51:42', null), ('15', '60', 'AB+', '0123456789G', '0123456789', '1989-07-01', 'Hombre', 'Calle Irreal nº 7', 'Desembarco del Rey', '01234', '2017-07-08 20:55:10', null), ('16', '61', 'AB-', '0123456789H', '0123456789', '1989-08-01', 'Hombre', 'Calle Irreal nº 8', 'Lanza del Sol', '01234', '2017-07-08 20:59:09', null);
COMMIT;

-- ----------------------------
--  Table structure for `enfermeros`
-- ----------------------------
DROP TABLE IF EXISTS `enfermeros`;
CREATE TABLE `enfermeros` (
  `id_enfermero` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_centro` int(10) unsigned NOT NULL,
  `id_usuario` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_enfermero`),
  KEY `id_centros` (`id_centro`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `enfermeros_ibfk_1` FOREIGN KEY (`id_centro`) REFERENCES `centros` (`id_centro`),
  CONSTRAINT `enfermeros_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `enfermeros`
-- ----------------------------
BEGIN;
INSERT INTO `enfermeros` VALUES ('8', '4', '44', '2017-07-08 20:07:11', null), ('9', '4', '45', '2017-07-08 20:10:37', null), ('10', '4', '46', '2017-07-08 20:12:45', null), ('12', '5', '48', '2017-07-08 20:13:22', '2017-07-08 20:16:17'), ('13', '5', '49', '2017-07-08 20:14:34', '2017-07-08 20:16:02'), ('14', '5', '50', '2017-07-08 20:15:25', '2017-07-08 20:15:46'), ('15', '10', '51', '2017-07-08 20:17:24', null), ('16', '10', '52', '2017-07-08 20:18:16', null), ('17', '10', '53', '2017-07-08 20:20:03', null);
COMMIT;

-- ----------------------------
--  Table structure for `reservas`
-- ----------------------------
DROP TABLE IF EXISTS `reservas`;
CREATE TABLE `reservas` (
  `id_reserva` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_centro` int(10) unsigned NOT NULL,
  `cantidad` varchar(255) DEFAULT NULL,
  `grupo_sanguineo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_reserva`),
  KEY `id_centro` (`id_centro`),
  CONSTRAINT `reservas_ibfk_1` FOREIGN KEY (`id_centro`) REFERENCES `centros` (`id_centro`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `reservas`
-- ----------------------------
BEGIN;
INSERT INTO `reservas` VALUES ('25', '4', '4', '0-', '2017-07-08 19:19:58', '2017-07-09 00:16:11'), ('26', '4', '10', '0+', '2017-07-08 19:19:58', '2017-07-09 00:12:14'), ('27', '4', '10', 'A-', '2017-07-08 19:19:58', '2017-07-08 21:14:17'), ('28', '4', '10', 'A+', '2017-07-08 19:19:58', '2017-07-08 21:32:35'), ('29', '4', '10', 'B-', '2017-07-08 19:19:58', '2017-07-08 21:29:32'), ('30', '4', '3', 'B+', '2017-07-08 19:19:58', '2017-07-09 00:26:45'), ('31', '4', '7', 'AB-', '2017-07-08 19:19:58', '2017-07-09 00:19:47'), ('32', '4', '10', 'AB+', '2017-07-08 19:19:58', '2017-07-08 21:18:35'), ('33', '5', '6', '0-', '2017-07-08 19:28:25', '2017-07-09 00:13:50'), ('34', '5', '10', '0+', '2017-07-08 19:28:25', '2017-07-08 21:37:23'), ('35', '5', '10', 'A-', '2017-07-08 19:28:25', '2017-07-08 21:40:52'), ('36', '5', '10', 'A+', '2017-07-08 19:28:25', '2017-07-08 21:12:23'), ('37', '5', '10', 'B-', '2017-07-08 19:28:25', '2017-07-08 21:17:48'), ('38', '5', '1', 'B+', '2017-07-08 19:28:25', '2017-07-09 00:24:27'), ('39', '5', '2', 'AB-', '2017-07-08 19:28:25', '2017-07-09 00:20:57'), ('40', '5', '10', 'AB+', '2017-07-08 19:28:25', '2017-07-08 21:43:07'), ('73', '10', '3', '0-', '2017-07-08 19:58:39', '2017-07-09 00:17:09'), ('74', '10', '10', '0+', '2017-07-08 19:58:39', '2017-07-08 21:05:57'), ('75', '10', '10', 'A-', '2017-07-08 19:58:39', '2017-07-08 21:47:02'), ('76', '10', '10', 'A+', '2017-07-08 19:58:39', '2017-07-08 21:45:45'), ('77', '10', '10', 'B-', '2017-07-08 19:58:39', '2017-07-08 21:47:53'), ('78', '10', '4', 'B+', '2017-07-08 19:58:39', '2017-07-09 00:25:40'), ('79', '10', '5', 'AB-', '2017-07-08 19:58:39', '2017-07-09 00:23:01'), ('80', '10', '10', 'AB+', '2017-07-08 19:58:39', '2017-07-09 00:13:08');
COMMIT;

-- ----------------------------
--  Table structure for `transfusiones`
-- ----------------------------
DROP TABLE IF EXISTS `transfusiones`;
CREATE TABLE `transfusiones` (
  `id_transfusion` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_centro` int(10) unsigned NOT NULL,
  `cantidad` int(10) unsigned DEFAULT NULL,
  `grupo_sanguineo` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `observaciones` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_transfusion`),
  KEY `id_centro` (`id_centro`),
  CONSTRAINT `transfusiones_ibfk_1` FOREIGN KEY (`id_centro`) REFERENCES `centros` (`id_centro`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `transfusiones`
-- ----------------------------
BEGIN;
INSERT INTO `transfusiones` VALUES ('12', '5', '4', 'AB-', '2017-07-08', 'Accidente de tráfico', '2017-07-09 00:10:04', null), ('13', '5', '4', '0-', '2017-07-08', ' Transplante riñón', '2017-07-09 00:13:50', null), ('14', '4', '2', '0-', '2017-07-08', ' Parto natural', '2017-07-09 00:15:07', null), ('15', '4', '4', '0-', '2017-07-08', ' Accidente de tráfico', '2017-07-09 00:16:11', null), ('16', '10', '7', '0-', '2017-07-08', 'Transporte de sangre a otro centro', '2017-07-09 00:17:09', null), ('17', '4', '3', 'AB-', '2017-07-08', ' Transplante hígado', '2017-07-09 00:19:47', null), ('18', '5', '8', 'AB-', '2017-07-08', ' Transplante de corazón', '2017-07-09 00:20:57', null), ('19', '10', '5', 'AB-', '2017-07-08', 'Traslado a otro centro', '2017-07-09 00:23:01', null), ('20', '5', '9', 'B+', '2017-07-08', 'Transplante de corazón', '2017-07-09 00:24:27', null), ('21', '10', '6', 'B+', '2017-07-08', 'Accidente de tráfico', '2017-07-09 00:25:40', null), ('22', '4', '7', 'B+', '2017-07-08', ' Traslado a otro centro', '2017-07-09 00:26:45', null);
COMMIT;

-- ----------------------------
--  Table structure for `usuarios`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id_usuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `usuarios`
-- ----------------------------
BEGIN;
INSERT INTO `usuarios` VALUES ('43', 'Jose Lorenzo ', 'Moreno Moreno', 'info@admin.com', '$2y$10$gwFNNVpxwJWi52U0qwozV.0XEy5u6FpwM7Oc549K4bNIqnysI3HKW', 'administrador', '2017-07-08 19:12:51', null, null), ('44', 'John', 'Doe', 'johndoe@enfermeria.com', '$2y$10$QnSYRZWHBRFdVbguYizQyuZrQ2jFdJI7gAAYqzHOASvRWVZVAioI.', 'enfermero', '2017-07-08 20:07:11', '2017-07-08 20:20:22', null), ('45', 'Jane', 'Doe', 'janedoe@enfermeria.com', '$2y$10$cCGmL9aIy4x0ye1ei5XCzuaSaIRL.ycYRPiPD3clScZ0KvCPKUc4C', 'enfermero', '2017-07-08 20:10:37', null, null), ('46', 'Mike', 'Doe', 'mikedoe@enfermeria.com', '$2y$10$9VshGkIuSXqUe8TorxgpGOt3Oad3pviWJ8BXJpOrDRgwc6HMlamA2', 'enfermero', '2017-07-08 20:12:45', null, null), ('48', 'Agnes', 'Doe', 'agnesdoe@enfermeria.com', '$2y$10$02uDcQ.RcsABgucgk9kE6eUe15pVOWRHoOtImRkBPcrFPZr7TYceO', 'enfermero', '2017-07-08 20:13:22', null, null), ('49', 'Daniel', 'Doe', 'danieldoe@enfermeria.com', '$2y$10$Vm6xkHV6Z4jZbPMfLSDy0eX9ysk1kV4iNnOtZxWjHtvlbJkkeEhhe', 'enfermero', '2017-07-08 20:14:34', null, null), ('50', 'Mary', 'Doe', 'marydoe@enfermeria.com', '$2y$10$eJ3XGrIQnWs2xI/JFj877OHNwZCXn5J7QkZLdUPXVHxpYEeQrUyJy', 'enfermero', '2017-07-08 20:15:25', null, null), ('51', 'Terry', 'Doe', 'terrydoe@enfermeria.com', '$2y$10$sQRED89zycSRxztaI7TLcO0bqoUwvnHkNmNpj9hDo321iaMmtBJFS', 'enfermero', '2017-07-08 20:17:24', null, null), ('52', 'Marge', 'Doe', 'margedoe@enfermeria.com', '$2y$10$8/AlFkLaJWPh2NFa/AI60e70D4ssq3/sPOdTOzhnYRD8I8vA1x2/u', 'enfermero', '2017-07-08 20:18:16', null, null), ('53', 'Robin', 'Doe', 'robindoe@enfermeria.com', '$2y$10$r6Ijvufrap3OWKY9z1NAb.iQjcPT29sAUAxM2pt/yjvwUPM40LPZS', 'enfermero', '2017-07-08 20:20:03', null, null), ('54', 'Daenerys', 'Targaryen', 'danny@targaryen.com', '$2y$10$WJgU4zgxc5EFM3j/xO60leSYzzZnYcRWylYQ.ZjVa2s.eo6o92mVG', 'donante', '2017-07-08 20:27:02', '2017-07-08 21:53:13', 'GCJDyUcuyGXcfUh0a9bb357qlASmFbztTA1LYiYSDoj6xpxUGOaAYW1oCYOp'), ('55', 'Jon', 'Snow', 'jon@snow.com', '$2y$10$gwFNNVpxwJWi52U0qwozV.0XEy5u6FpwM7Oc549K4bNIqnysI3HKW', 'donante', '2017-07-08 20:38:03', '2017-07-08 21:57:44', 'gESIlerHOItQM6BQu2hyMljSHYr7rTuEbToTsk4gM6yLkkb8DoxCrus5EWX2'), ('56', 'Tyrion', 'Lannister', 'tyrion@lannister.com', '$2y$10$734fzVTSjYApeCnLFV5uH.TD1RFchEjCvqf9u9EnpFJbofXAHUVFa', 'donante', '2017-07-08 20:43:54', null, null), ('57', 'Margaery ', 'Tyrell', 'margaery@tyrell.com', '$2y$10$PMCBbYhpgykvfHOP7JEWbuMHW9XDDBwvpnZsGbxYvOMVaZXm60qfG', 'donante', '2017-07-08 20:47:09', null, null), ('58', 'Hodor', 'Hodor', 'hodor@hodor.hodor', '$2y$10$W84xPyVl0f0rCLMR4eJ9euRHdozTPyQTogfQTceu7AGEEjjVS.rvi', 'donante', '2017-07-08 20:48:25', null, null), ('59', 'Sansa', 'Stark', 'sansa@stark.com', '$2y$10$pyKEwvuF5F5uLjHk7cvWSumYwiBj1xuxlgomrOI7wYymqyOCQue/a', 'donante', '2017-07-08 20:51:42', null, null), ('60', 'Joffrey ', 'Baratheon', 'joffrey@baratheon', '$2y$10$dvjDe9mTP0ccYn4T5kOmFeealBCwKj4YKBm1fjU/V8VM3n32.cIU2', 'donante', '2017-07-08 20:55:10', null, null), ('61', 'Oberyn ', 'Martell', 'oberyn@martell.com', '$2y$10$1iF1kyWAPhSZuhio9ARy8uAdCy4dvWOlnl0wrZCnxTU/Tyu5AIsVe', 'donante', '2017-07-08 20:59:09', null, null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
