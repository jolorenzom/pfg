<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\Usuario;

class AdminController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');


    }
    
    public function index(){
        $usuario_logado = Auth::user();

    	$tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
        	return redirect('/home');
        }

		return view('admin.admin_home', compact('usuario_logado'));	
	}

    public function edit($id_usuario)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }

        $usuario = Usuario::find($id_usuario);
        
        if (!$usuario) {
            return redirect('/home')->with('alert', 'No existe el usuario');
        }
        return view('admin.admin_edicion_usuario', compact('usuario'));
    }

    public function update(Request $request, $id_usuario)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }

        $usuario = Usuario::find($id_usuario);

        $usuario -> nombre = $request -> nombre;
        $usuario -> apellidos = $request -> apellidos;
        $usuario -> email = $request -> email;
        if ($request -> password != "") 
        {
            $passhash = Hash::make($request -> password);
            $usuario -> password = $passhash;
        }
        $usuario -> save();

        return redirect('/home')->with('success', 'Datos editados');

    }
}