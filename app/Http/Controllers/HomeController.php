<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Donante;
use App\Centro;
use App\Enfermero;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $usuario_logado = Auth::user();
        $tipo_usuario = Auth::user() -> tipo;

        switch ($tipo_usuario) {
            case 'donante':
                $donante = Donante::where('id_usuario', $usuario_logado -> id_usuario)->first();
        
                return view('donante.donante_home', compact('usuario_logado','donante'));
            break;

            case 'administrador':
                return view('admin.admin_home', compact('usuario_logado')); 
            break;

            case 'enfermero':
                    $enfermero = Enfermero::where('id_usuario', $usuario_logado -> id_usuario)->first();
                    $centro = Centro::where('id_centro', $enfermero -> id_centro)->first();

                    return view('enfermero.enfermero_home', compact('usuario_logado','centro'));
            break;

            default:
                Auth::logout();
                return redirect('/');
            break;
        }
    }
}
