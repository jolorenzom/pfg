<?php
namespace App\Http\Controllers\Anonimo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AnonimoController extends Controller
{

	public function index()
	{
		return view('welcome');
	}

	public function logout()
	{
		Auth::logout();
		return redirect('/');
	}
}

