<?php

namespace App\Http\Controllers\donaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Donacion;
use App\Usuario;
use App\Donante;
use App\Centro;
use App\Reserva;
use DB;
use Hash;

class DonacionesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
        	return view('donaciones.donaciones');
        }else      
        return redirect('/home');
				
	}

    public function GetDonacionesDonanteByUser()
    {
        $usuario_logado = Auth::user();

        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'donante') {
            return redirect('/home');
        }

        $donante = Donante::where('id_usuario', '=', $usuario_logado -> id_usuario)->first();

        $donaciones = Donacion::where('id_donante', '=', $donante -> id_donante)->get();

        if ($donaciones == "") 
        {
            return redirect('/home');
        }
        
        
        foreach ($donaciones as $donacion) 
        {
            $centros = Centro::where('id_centro', '=', $donacion -> id_centro)->first();
            $donacion -> nombre_centro = $centros -> nombre;
        }

        $donacionAux1 = Donacion::where('id_donante', '=', $donante -> id_donante)->orderBy('fecha', 'desc')->first();
        $fecha_actual = date('Y-m-d');
        if($donacionAux1 != "")
        {
            $dias = (strtotime($fecha_actual)-strtotime($donacionAux1 -> fecha))/86400;
            $dias = abs($dias);
            $dias = floor($dias);
            if($dias < 56)
                return view('donante.donaciones.donante_donaciones', compact('donaciones'))->with('alert', 'No se puede donar: Han pasado menos de 8 semanas desde la última vez');  
        }

        $contador = 0;
        $donacionesAux2 = Donacion::where('id_donante', '=', $donante -> id_donante)->orderBy('fecha', 'desc')->get();
        if ($donacionesAux2 != "")
        {
            foreach ($donacionesAux2 as $donacionAux2) 
            {
                $fechaDividida = explode("-", $fecha_actual);
                $annoRequest = $fechaDividida[0];
                $fechaDividida = explode("-", $donacion -> fecha);
                $annoDonacion = $fechaDividida[0];

                if ($annoDonacion == $annoRequest )
                {
                    $contador++;
                }
            }

            if ($donante -> sexo == 'Mujer' && $contador >= 3) 
            {
                return view('donante.donaciones.donante_donaciones', compact('donaciones'))->with('alert', 'Mujer -> Ha donado el maximo de veces permitidas en un año.');              
            }

            if ($donante -> sexo == 'Hombre' && $contador >= 4) 
            {
                return view('donante.donaciones.donante_donaciones', compact('donaciones'))->with('alert', 'Hombre -> Ha donado el maximo de veces permitidas en un año.');               
            }            
        }

        return view('donante.donaciones.donante_donaciones', compact('donaciones'))->with('success', 'Puede donar');        
    }

    public function GetDonacionesDonanteByDNI(Request $request)
    {
       $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){

            if ($request -> dni == "")
            {
                return redirect('/donaciones')->with('message', 'Debe introducir un dni');
            }

            $donante = Donante::where('dni', '=', $request -> dni)->first();
            if ($donante == "") 
            {
                return redirect('/donaciones')->with('alert', 'No se ha encontrado ningún donante con el dni dado');
            }

            $usuario = Usuario::find($donante -> id_usuario);

            $donaciones = Donacion::where('id_donante', '=', $donante -> id_donante)->orderBy('fecha', 'desc')->get();

            foreach ($donaciones as $donacion) 
            {
                $centro = Centro::where('id_centro', '=', $donacion -> id_centro)->first();
                $donacion -> nombre_centro = $centro -> nombre;
            }

            $centros = Centro::all(); 

            $fecha_actual = date("Y-m-d"); 

            return view('donaciones.donaciones_donante', compact('donaciones','donante', 'usuario', 'centros', 'fecha_actual'));
        }else
            return redirect('/home');
    }

    public function store(Request $request)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
            $donacionAux1 = Donacion::where('id_donante', '=', $request -> id_donante)->orderBy('fecha', 'desc')->first();

            if($donacionAux1 != "")
            {
                $dias = (strtotime($request -> fecha)-strtotime($donacionAux1 -> fecha))/86400;
                $dias = abs($dias);
                $dias = floor($dias);

                if($dias < 56)
                    return redirect('/donaciones')->with('alert', 'No se puede donar: Han pasado menos de 8 semanas desde la ultima vez');
            }
            
            $donacion = new Donacion;
            $donacion -> cantidad = $request -> cantidad;
            $donacion -> id_centro = $request -> id_centro;
            $donacion -> id_donante = $request -> id_donante;        
            $donacion -> fecha = $request -> fecha;
            $donacion -> grupo_sanguineo = $request -> grupo_sanguineo;

            $contador = 0;
            $donacionesAux2 = Donacion::where('id_donante', '=', $request -> id_donante)->orderBy('fecha', 'desc')->get();
            if ($donacionesAux2 != "")
            {
                foreach ($donacionesAux2 as $donacionAux2) 
                {
                    $fechaDividida = explode("-", $request -> fecha);
                    $annoRequest = $fechaDividida[0];
                    $fechaDividida = explode("-", $donacion -> fecha);
                    $annoDonacion = $fechaDividida[0];

                    if ($annoDonacion == $annoRequest )
                    {
                        $contador++;
                    }
                }
                $donante = Donante::find($request -> id_donante)->first();
                if ($donante -> sexo == 'Mujer' && $contador >= 3) 
                {
                    return redirect('/donaciones')->with('alert', 'Mujer -> Ha donado el maximo de veces permitidas en un año.');            
                }

                if ($donante -> sexo == 'Hombre' && $contador >= 4) 
                {
                    return redirect('/donaciones')->with('alert', 'Hombre -> Ha donado el maximo de veces permitidas en un año.');            
                }            
            }
             
            $donacion -> save();

            $reservas = Reserva::all();
            foreach ($reservas as $reserva) 
            {
                if(($reserva -> id_centro == $request -> id_centro) and ($reserva -> grupo_sanguineo == $donacion -> grupo_sanguineo))
                {
                    $reserva -> cantidad = $reserva -> cantidad + $request -> cantidad;
                    $reserva -> save();
                }  
            }

            return redirect('/donaciones')->with('success', 'Donación realizada con éxito');           
        }else
            return redirect('/home');
    }
}
