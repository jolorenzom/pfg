<?php

namespace App\Http\Controllers\transfusiones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Transfusion;
use App\Centro;
use App\Reserva;
use DB;

class TransfusionesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
        	$transfusiones = Transfusion::all();        
            return view('transfusiones.transfusiones', compact('transfusiones'));
        }else
            return redirect('/home');        		
	}

	public function create()
    {
        $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
            $centros = Centro::all();
            if ($centros == "") 
            {
                return redirect('/transfusiones')->with('message', 'No se puede dar de alta: No existen centros de transfusión');
            }
            $fecha_actual = date("Y-m-d"); 

            return view('transfusiones.alta_transfusion', compact('centros','fecha_actual'));
        }else
            return redirect('/home');
    }

    public function store(Request $request)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
            $transfusion = new Transfusion;
            $transfusion -> id_centro = $request -> id_centro;
            $transfusion -> cantidad = $request -> cantidad;
            $transfusion -> grupo_sanguineo = $request -> grupo_sanguineo;
            $transfusion -> fecha = $request -> fecha;
            $transfusion -> observaciones = $request -> observaciones;
            $transfusion -> save();

            //TODO: refactorizar para traer una sola reserva 
            //y poder hacer la comprobacion de si se queda en negativo
            //Hacer el save() para la transfusion y la reserva si todo ha ido bien
            //si no, mostrar mensaje de error
            $reservas = Reserva::all();
            foreach ($reservas as $reserva) 
            {
                if(($reserva -> id_centro == $request -> id_centro) and ($reserva -> grupo_sanguineo == $request -> grupo_sanguineo))
                {
                    $reserva -> cantidad = $reserva -> cantidad - $request -> cantidad;
                    $reserva -> save(); 
                }  
            }
            return redirect('/alta_transfusion')->with('success', 'Transfusión realizada con éxito');
        } else
            return redirect('/home');

    }
}
