<?php

namespace App\Http\Controllers\enfermeros;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Enfermero;
use App\Usuario;
use App\Centro;
use DB;
use Hash;

class EnfermerosController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
        	return redirect('/home');
        }        
    
        $centros = Centro::all();            

		return view('enfermeros.enfermeros', compact('centros'));		
	}

    public function GetEnfermerosByIdCentro(Request $request)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }

        $centro = Centro::find($request -> id_centro);

        $enfermeros = DB::table('enfermeros')
            ->select('*')
            ->join('usuarios', 'enfermeros.id_usuario', '=', 'usuarios.id_usuario')
            ->where('id_centro', '=', $request -> id_centro)
            ->get();

        if (count($enfermeros) == 0) 
        {
            return redirect('/enfermeros')->with('alert', 'No existen enfermeros para este centro');
        }

        return view('enfermeros.enfermeros_list', compact('centro', 'enfermeros'));
    }

    public function GetEnfermerosFiltrados(Request $request)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }

        if (($request -> nombre == "") and ($request -> apellidos == "") and ($request -> email == ""))
        {
            return redirect('/enfermeros')->with('message', 'Todos los campos están vacíos - Rellene al menos uno de ellos');
        }

        $enfermeros = DB::table('enfermeros')
            ->select('*')
            ->join('usuarios', 'enfermeros.id_usuario', '=', 'usuarios.id_usuario')
            ->where('usuarios.nombre', 'LIKE', "%{$request -> nombre}%" )
            ->where('usuarios.apellidos', 'LIKE', "%{$request -> apellidos}%")
            ->where('usuarios.email', 'LIKE', "%{$request -> email}%")
            ->get();

        if (count($enfermeros) == 0) 
        {
            return redirect('/enfermeros')->with('alert', 'No existen enfermeros que coincidan con los datos introducidos');
        }

        $centro = new Centro;

        return view('enfermeros.enfermeros_list', compact('centro','enfermeros'));
    }


	public function GetEnfermeroById($id_enfermero)
    {

        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }
       

        $enfermero = Enfermero::find($id_enfermero);
        
        if (!$enfermero) {
            return redirect('/enfermeros')->with('alert', 'No existe el enfermero');
        }

        $usuario = Usuario::find($enfermero -> id_usuario);

        if (!$usuario) {
            abort(503);
        }

        $centro = Centro::find($enfermero -> id_centro);

        return view('enfermeros.enfermero', compact('enfermero', 'usuario', 'centro')); 
    }

    public function create()
    {
        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }

        $centros = Centro::all();
        if ($centros == "") 
        {
            return redirect('/enfermeros')->with('alert', 'No se puede dar de alta: No existen centros de donación');
        }

        return view('enfermeros.alta_enfermero', compact('centros'));
    }

    public function store(Request $request)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }

        $password = str_random(8);
        $passhash = Hash::make($password);
        $tipo = "enfermero";

        $usuario = new Usuario;
        $usuario -> nombre = $request -> nombre;
        $usuario -> apellidos = $request -> apellidos;
        $usuario -> email = $request -> email;
        $usuario -> password = $passhash;
        $usuario -> tipo = $tipo;

        $user_find1 = Usuario::where('email', '=', $request -> email)->first();

        if ($user_find1 != "") 
        {   
            return redirect('/enfermeros')->with('alert', 'ERROR -> Email repetido');
        }

        $usuario -> save();

        $enfermero = new Enfermero;
        $enfermero -> id_usuario = $usuario -> id_usuario;  
        $enfermero -> id_centro = $request -> id_centro;  
        $enfermero -> save();

        return redirect('/enfermeros')->with('success', 'Enfermero creado correctamente');
    }

    public function edit($id_enfermero)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }

        $enfermero = Enfermero::find($id_enfermero);

        if (!$enfermero) {
            return redirect('/enfermeros')->with('alert', 'No existe el enfermero');
        }

        $usuario = Usuario::find($enfermero -> id_usuario);

        if (!$usuario) {
            abort(503);
        }    

        $centros = Centro::all();

        return view('enfermeros.edicion_enfermero', compact('enfermero','usuario','centros'));
    }

    public function update(Request $request, $id_enfermero)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }

        $enfermero = Enfermero::find($id_enfermero);
        $usuario = Usuario::find($enfermero -> id_usuario);

        $tipo = "enfermero";

        $usuario -> nombre = $request -> nombre;
        $usuario -> apellidos = $request -> apellidos;
        $usuario -> email = $request -> email;
        if ($request -> password != "") 
        {
            $passhash = Hash::make($request -> password);
            $usuario -> password = $passhash;
        }
        $usuario -> tipo = $tipo;
        $usuario -> save();
        $enfermero -> id_usuario = $usuario -> id_usuario; 
        if($request -> id_centro != "vacio")        
            $enfermero -> id_centro = $request -> id_centro;  
        $enfermero -> save();

        return redirect('/enfermeros')->with('success', 'Datos editados');
    }

    public function destroy($id_enfermero)
    {

        $tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
            return redirect('/home');
        }

        $enfermero = Enfermero::find($id_enfermero);

        if (!$enfermero) {
            return redirect('/enfermeros')->with('alert', 'No existe el enfermero');
        }

        $usuario = Usuario::find($enfermero -> id_usuario);

        if (!$usuario) {
            return redirect('/enfermeros')->with('alert', 'No existe el enfermero');
        }

        $enfermero -> delete();
        $usuario -> delete();

        return redirect('/enfermeros')->with('success', 'Enfermero eliminado');
    }
}
