<?php

namespace App\Http\Controllers\donantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Donante;
use App\Usuario;
use App\Donacion;
use App\Reserva;
use DB;
use Hash;
use Mail;

class DonantesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
        	return view('donantes.donantes');
        }else
            return redirect('/home');
				
	}

    public function GetDonantesFiltrados(Request $request)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
            if (($request -> nombre == "") and ($request -> apellidos == "") and ($request -> dni == ""))
            {
                return redirect('/donantes')->with('message', 'Todos los campos están vacíos - Rellene al menos uno de ellos');
            }

            $donantes = DB::table('donantes')
                ->select('*')
                ->join('usuarios', 'donantes.id_usuario', '=', 'usuarios.id_usuario')
                ->where('usuarios.nombre', 'LIKE', "%{$request -> nombre}%" )
                ->where('usuarios.apellidos', 'LIKE', "%{$request -> apellidos}%")
                ->where('donantes.dni', 'LIKE', "%{$request -> dni}%")
                ->get();

            if (count($donantes) == 0) 
            {
                return redirect('/donantes')->with('alert', 'No existen donantes que coincidan con los datos introducidos');
            }

            return view('donantes.donantes_filtrados', compact('donantes'));    
        }else  
            return redirect('/home');

    
    }

    public function GetDonanteById($id_donante)
    {

        $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
            $donante = Donante::find($id_donante);
            
            if (!$donante) {
                return redirect('/donantes')->with('alert', 'No existe el donante');
            }

            $usuario = Usuario::find($donante -> id_usuario);

            if (!$usuario) {
                abort(503);
            }

            $fecha_nacimiento = explode("-", $donante -> fecha_nacimiento);

            $donante -> fecha_nacimiento = $fecha_nacimiento[2]."-".$fecha_nacimiento[1]."-".$fecha_nacimiento[0];

            return view('donantes.donante', compact('donante','usuario'));  
        }else
            return redirect('/home');  
    }

    public function create()
    {
        $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
            return view('donantes.alta_donante');
        }else
            return redirect('/home');
        
    }

    public function store(Request $request)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero')){
            $password = str_random(8);
            $passhash = Hash::make($password);
            $tipo = "donante";

            $usuario = new Usuario;
            $usuario -> nombre = $request -> nombre;
            $usuario -> apellidos = $request -> apellidos;
            $usuario -> email = $request -> email;
            $usuario -> password = $passhash;
            $usuario -> tipo = $tipo;

            $user_find1 = Usuario::where('email', '=', $request -> email)->first();

            if ($user_find1 != "") 
            {   
                return redirect('/donantes')->with('alert', 'ERROR -> Email repetido');
            }

            $user_find2 = Donante::where('dni', '=', $request -> dni)->first();
            
            if ($user_find2 != "") 
            {   
                return redirect('/donantes')->with('alert', 'ERROR -> DNI repetido');
            }

            $usuario -> save();

            $donante = new Donante;
            $donante -> id_usuario = $usuario -> id_usuario;        
            $donante -> grupo_sanguineo = $request -> grupo_sanguineo;
            $donante -> dni = $request -> dni;
            $donante -> telefono = $request -> telefono;
            $donante -> fecha_nacimiento = $request -> fecha_nacimiento;
            $donante -> sexo = $request -> sexo;
            $donante -> direccion = $request -> direccion;
            $donante -> municipio = $request -> municipio;
            $donante -> cp = $request -> cp;
            $donante -> save(); 

            $data=['nombre'=> $usuario -> nombre, 'apellidos'=> $usuario -> apellidos, 'password' => $password, 'email' => $usuario -> email, 'tipo' => $usuario -> tipo, 'grupo_sanguineo' => $donante -> grupo_sanguineo, 'dni' => $donante -> dni, 'telefono' => $donante -> telefono, 'fecha_nacimiento' => $donante -> fecha_nacimiento, 'sexo' => $donante -> sexo, 'direccion' => $donante -> direccion, 'municipio' => $donante -> municipio, 'cp' => $donante -> cp];
            
            Mail::send('email.email_template', $data, function($message) use ($usuario)
            {

                $message->to($usuario -> email, 'Donante');
                $message->subject('Alta de donante - datos del donante');
                $message->from('jolorenzom@gmail.com','Administrador');
            });

            return redirect('/donantes')->with('success', 'Donante creado correctamente');
        }else
            return redirect('/home');        
    }

    public function edit($id_donante)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero') or ($tipo_usuario == 'donante')){
            
            $donante = Donante::find($id_donante);

            if (!$donante) {
                if ($tipo_usuario == 'donante') {
                    return redirect('/home')->with('alert', 'No existe el usuario');
                }
                return redirect('/donantes')->with('alert', 'No existe el donante');
            }

            $usuario = Usuario::find($donante -> id_usuario);

            if (!$usuario) {
                abort(503);
            }     

            return view('donantes.edicion_donante', compact('donante','usuario'));
        }else
            return redirect('/home');

            
    }

    public function update(Request $request, $id_donante)
    {
        $tipo_usuario = Auth::user() -> tipo;

        if (($tipo_usuario == 'administrador') or ($tipo_usuario == 'enfermero') or ($tipo_usuario == 'donante')){
            $donante = Donante::find($id_donante);
            $usuario = Usuario::find($donante -> id_usuario);

            $usuario -> nombre = $request -> nombre;
            $usuario -> apellidos = $request -> apellidos;
            $usuario -> email = $request -> email;
            if ($request -> password != "") 
            {
                $password = $request -> password;
                $passhash = Hash::make($request -> password);
                $usuario -> password = $passhash;
            }else
                $password = 'La contraseña no se ha cambiado';

            $tipo = "donante";
            $usuario -> tipo = $tipo;
            $usuario -> save();

            $donante -> id_usuario = $usuario -> id_usuario;        
            $donante -> dni = $request -> dni;
            $donante -> telefono = $request -> telefono;
            $donante -> fecha_nacimiento = $request -> fecha_nacimiento;
            if($request -> sexo != "vacio") 
                $donante -> sexo = $request -> sexo;
            $donante -> direccion = $request -> direccion;
            $donante -> municipio = $request -> municipio;
            $donante -> cp = $request -> cp;
            $donante -> save();

            $data=['nombre'=> $usuario -> nombre, 'apellidos'=> $usuario -> apellidos, 'password' => $password, 'email' => $usuario -> email, 'tipo' => $usuario -> tipo, 'grupo_sanguineo' => $donante -> grupo_sanguineo, 'dni' => $donante -> dni, 'telefono' => $donante -> telefono, 'fecha_nacimiento' => $donante -> fecha_nacimiento, 'sexo' => $donante -> sexo, 'direccion' => $donante -> direccion, 'municipio' => $donante -> municipio, 'cp' => $donante -> cp];
            
            Mail::send('email.email_template', $data, function($message) use ($usuario)
            {

                $message->to($usuario -> email, 'Donante');
                $message->subject('Edición de donante - datos del donante');
                $message->from('jolorenzom@gmail.com','Administrador');
            });

            if ($tipo_usuario== 'donante') {
                return redirect('home')->with('success', 'Datos editados');
            }
            return redirect('/donantes')->with('success', 'Datos editados');
        }else
            return redirect('/home');            
    }
}