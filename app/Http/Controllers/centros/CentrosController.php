<?php

namespace App\Http\Controllers\centros;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Centro;
use App\Usuario;
use App\Enfermero;
use App\Donacion;
use App\Transfusion;
use App\Reserva;


class CentrosController extends Controller
{
	public function index()
    {
    	$usuario = Auth::user();

    	if($usuario=="") {
    		$usuario = new Usuario;
    		$usuario->tipo = 'anonimo';
    	}

    	$centros = Centro::all();

    	if ($centros == "") 
        {
            return redirect('/')->with('message', 'No existen centros');
        }

        return view('centros.centros', compact('centros','usuario'));
	}

	public function GetCentroById($id_centro)
	{
		$centro = Centro::find($id_centro);
		if ($centro == "") 
	    {
	        return redirect('/centros')->with('alert', 'No existe el centro seleccionado');
	    }

		return view('centros.centro', compact('centro'));
	}

	public function create()
	{
		$tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
        	return redirect('/home');
        }

        return view('centros.admin_alta_centro');
	}

	public function store(Request $request)
	{
		$tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
        	return redirect('/home');
        }

        $centro = new Centro;

        $centro -> nombre = $request->nombre;
        $centro -> direccion = $request->direccion;
        $centro -> municipio = $request->municipio;
        $centro -> cp = $request->cp;
        $centro -> telefono = $request->telefono;
        $centro -> horario_donaciones = $request->horario_donaciones;
        $centro -> ubicacion_sala_donaciones = $request->ubicacion_sala_donaciones;
        $centro -> telefono_donaciones = $request->telefono_donaciones;
        $centro -> email = $request->email;
        $centro -> web = $request->web;
        $centro -> observaciones = $request->observaciones;
        $centro -> save();

        //Inicialización de las reservas de sangre de ese centro
        $grupos_sanguineos = array("0-","0+","A-","A+","B-","B+","AB-","AB+");
        foreach ($grupos_sanguineos as $grupo_sanguineo) {
            $reserva = new Reserva;
            $reserva -> id_centro = $centro -> id_centro;    
            $reserva -> cantidad = 0;
            $reserva -> grupo_sanguineo = $grupo_sanguineo;           
            $reserva -> save();
        }        

        return redirect('/centros')->with('success', 'Centro creado correctamente');
	}

	public function edit($id_centro)
	{
		$tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
        	return redirect('/home');
        }

		$centro = Centro::find($id_centro);
		if (!$centro) {
			return redirect('/centros')->with('alert', 'No existe el centro seleccionado');
		}
		return view('centros.admin_edicion_centro', compact('centro'));
	}

	public function update(Request $request, $id_centro)
	{
		$tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
        	return redirect('/home');
        }

		$centro = Centro::find($id_centro);
		
        $centro -> nombre = $request->nombre;
        $centro -> direccion = $request->direccion;
        $centro -> municipio = $request->municipio;
        $centro -> cp = $request->cp;
        $centro -> telefono = $request->telefono;
        $centro -> horario_donaciones = $request->horario_donaciones;
        $centro -> ubicacion_sala_donaciones = $request->ubicacion_sala_donaciones;
        $centro -> telefono_donaciones = $request->telefono_donaciones;
        $centro -> email = $request->email;
        $centro -> web = $request->web;
        $centro -> observaciones = $request->observaciones;

        $centro -> save();

        return redirect('/centros')->with('success', 'Centro editado correctamente');
	}

	public function destroy($id_centro)
	{

		$tipo_usuario = Auth::user() -> tipo;

        if ($tipo_usuario != 'administrador') {
        	return redirect('/home');
        }
        
        $enfermeros = Enfermero::all();
        foreach ($enfermeros as $enfermero)
        {
            if ($enfermero -> id_centro == $id_centro) 
            {
                $usuario = Usuario::find($enfermero -> id_usuario);
                $enfermero -> delete();
                $usuario -> delete();
            }
        }

        $reservas = Reserva::all();
        foreach ($reservas as $reserva) 
        {
            if ($reserva -> id_centro == $id_centro) 
            {
                $reserva ->delete();
            }
        }

        $donaciones = Donacion::all();
        foreach ($donaciones as $donacion) 
        {
            if ($donacion -> id_centro == $id_centro) 
            {
                $donacion -> delete();
            }
        }

        $transfusiones = Transfusion::all();
        foreach ($transfusiones as $transfusion) 
        {
            if ($transfusion -> id_centro == $id_centro) 
            {
                $transfusion -> delete();
            }
        }

		$centro = Centro::find($id_centro);

		$centro -> delete();

		return redirect('/centros');
	}
    
}
