<?php

namespace App\Http\Controllers\Anonimo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Centro;
use App\Reserva;

class AnonimoReservasController extends Controller
{
	public function GetAllReservas()
    {
        $reservas = Reserva::all();
        if ($reservas == "") 
        {
            return redirect('/')->with('message', 'No existen reservas de sangre');
        }

        $grupo_sanguineos = array("0+","A+","AB+","B+","0-","A-","AB-","B-");
        $cantidades = array();

        foreach ($grupo_sanguineos as $grupo_sanguineo) 
        {
            $cantidades[] = Reserva::where('grupo_sanguineo', '=', $grupo_sanguineo)->sum('cantidad');           
        }

        foreach ($reservas as $reserva) 
        {
            $centros = Centro::where('id_centro', '=', $reserva -> id_centro)->first();
            $reserva -> nombre_centro = $centros -> nombre;
        }

        $centros = Centro::all();

		return view('anonimo.reservas.anonimo_reservas', compact('reservas', 'cantidades', 'grupo_sanguineos', 'centros'));			
	}

	public function GetReservaByCentroID(Request $request)
    {
        $reservas = Reserva::where('id_centro', '=', $request -> id_centro)->orderBy('grupo_sanguineo', 'asc')->get();
        if ($reservas == "") 
        {
            return redirect('/reservas')->with('message', 'No existen reservas de sangre para este centro');
        }

        $centro = Centro::where('id_centro', '=', $request -> id_centro)->first();
        $reservas -> nombre_centro = $centro -> nombre;

        return view('anonimo.reservas.anonimo_reservas_centro', compact('reservas')); 
    }
}
