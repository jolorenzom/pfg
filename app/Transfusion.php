<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfusion extends Model
{
    protected $table = 'transfusiones';
    protected $primaryKey = 'id_transfusion';
    public $timestamps = false;
}
