<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Enfermero extends Model
{
    protected $table = 'enfermeros';
    protected $primaryKey = 'id_enfermero';
    public $timestamps = false;
}
